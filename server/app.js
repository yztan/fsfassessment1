/** Server Side Code  */

console.log("Starting at ....");
var express = require("express");
var bodyParser = require("body-parser");

var app = express();
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

console.log(__dirname + "/../client/");
const NODE_PORT= process.env.NODE_PORT || 3000;
app.use(express.static(__dirname + "/../client/"));

app.post("/users", function(req, res) {
    console.log("Received user object " + req.body);
    console.log("Received user object " + JSON.stringify(req.body));
    var user = req.body;
    console.log("Registering profile...")
    console.log("email > " + user.email);
    console.log("password > " + user.password);
    console.log("confirm password > "+ user.confirm_password);
    console.log("fullname > " + user.fullname);
    console.log("DOB > " + user.dateOfBirth);
    console.log("Address > " + user.address);
    console.log("Contact > " + user.contactNumber);
    console.log("Gender >  " + user.gender);
    console.log("Nationality >  " + user.nationality);
    res.status(200).json(user);
});


app.use(function(req, res) {
    res.send("<h1>Page not found! </h1>");
});

app.listen(NODE_PORT, function() {
    console.log("Staring web at " + NODE_PORT);
});