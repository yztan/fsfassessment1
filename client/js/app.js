/**
 * Client side code.
 */
(function() {
    var app = angular.module("RegApp", []);

    app.controller("RegCtrl", ["$http", RegCtrl]);

    function RegCtrl($http) {
        var self = this; // vm

        self.user = {
            email: "",
            password: "",
            confirm_password: "",
            fullname: "",
            dateOfBirth: "",
            address: "",
            contactNumber: "",
            gender: "",
            nationality: "",
            
        };

        self.displayUser = {
            email: "",
            password: "",
            confirm_password: "",
            fullname: "",
            dateOfBirth: "",
            address: "",
            contactNumber: "",
            gender: "",
            nationality: "",    
            
        };

   
        self.ageValid = function(dateOfBirth) {            
            var minAge = 18;
             console.log ("dateofbirth>>>" + dateOfBirth);
             var ageDifMs = Date.now() - dateOfBirth.getTime();
             var ageDate = new Date(ageDifMs); // miliseconds from epoch
             var ageYear = Math.abs(ageDate.getUTCFullYear() - 1970);
             
            console.log(ageYear);
             if(ageYear >= minAge){
                 return true;
           
                
             } else {
                 return false
                   
             }
         };

              

        self.registerUser = function() {
            console.log(self.user.email);
            console.log(self.user.password);
            console.log(self.user.confirm_password);
            console.log(self.user.fullname);
            console.log(self.user.dateOfBirth);
            console.log(self.user.address);
            console.log(self.user.contactNumber);
            console.log(self.user.gender);
            console.log(self.user.nationality);
      
            $http.post("/users", self.user)
                .then(function(result) {
                    console.log(result);
                    self.displayUser.email = result.data.email;
                    self.displayUser.password = result.data.password;
                    self.displayUser.confirm_password = result.data.confirm_password;
                    self.displayUser.fullname = result.data.fullname;
                    self.displayUser.dateOfBirth = result.data.dateOfBirth;
                    self.displayUser.address = result.data.address;
                    self.displayUser.contactNumber = result.data.contactNumber;
                    self.displayUser.gender = result.data.gender;
                    self.displayUser.nationality = result.data.nationality;
                }).catch(function(e) {
                    console.log(e);
                });
        };
    }

})();
