## Prepare the git and initialize first version
1. cd <directory>
1. mkdir <project folder>
1. cd <project folder>
1. git init
1. git remote add origin <http url name>
1. mkdir server
1. cd server
1. touch app.js
1. cd ..
1. git add .
1. git commit -m "initial version"
1. git push origin master

## Prepare the package required for the code

1. Create a file and name it .bowerrc
1. Inside the file, add the directory you want to save the bower compoents {
     "directory": "client/bower_components"
}
1. Create git .gitignore
1. Create package.json with npm init 
1. npm install
1. npm install bower
1. npm install body-parser
1. bower init
1. bower install angular bootstrap angular fontawesome --save

## Create the client side code 
1. Create a html page with the skeleton of the registration page
1. Create main.css to perform CSS style for validation fonts such errors
1. Create an app.js in the client side folder
1. Create a module in the app.js and a controller for the client side page
1. In the app.js in the client side, create a function that is able to register the details.
1. On the html side, use ng-cloak <body ng-cloak> to mask the undesirable template display
1. Use ng form states to perform validation results 


## Create Server side code
1. Import express module into the app.js in the server folder by 
var express = require("express");
1. Import body-parser module in the same app.js by 
var bodyParser = require(body-parser")
1. Enable the use of express module and usage of PORT 3000.

var app = express();
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

console.log(__dirname + "/../client/");
const NODE_PORT= process.env.NODE_PORT || 3000;
app.use(express.static(__dirname + "/../client/"));

app.use(function(req, res) {
    res.send("<h1>!!!! Page not found ! ! </h1>");
});

app.listen(NODE_PORT, function() {
    console.log("Web App started at " + NODE_PORT);
});

1. Create a function to allow posting. Add a console log to ensure that the data is received on the express console.

app.post("/users", function(req, res) {
    console.log("Received user object " + req.body);
    console.log("Received user object " + JSON.stringify(req.body));
    var user = req.body;
    res.status(200).json(user);
});

## Test it on browser
1. Go to server folder and activate nodemon
    cd server 
    nodemon app.js
1. Go on google browser and type localhost:3000 to test the page